from django.urls import path
from . import views

appname='jadwal'
urlpatterns = [
    path('', views.jadwal,name='jadwal'),
]
