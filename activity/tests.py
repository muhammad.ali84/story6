from django.test import TestCase,Client
from django.urls import resolve
from .views import jadwal
from .models import Kegiatan
from .models import peserta
# Create your tests here
class unitTest(TestCase):
    def test_app_url_exist(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)
    def test_app_jadwal(self):
        found=resolve('/')
        self.assertEqual(found.func, jadwal)
    def test_model1_cek(self):
        Kegiatan.objects.create(nama='pewe')
        hitungjumlah = Kegiatan.objects.all().count()
        self.assertEqual(hitungjumlah,1)
    def test_model2_cek(self):
        peserta.objects.create(nama='pewe',kegiatan=Kegiatan.objects.create(nama='dek'))
        hitungjumlah = peserta.objects.all().count()
        self.assertEqual(hitungjumlah,1)
    def test_model_name(self):
        Kegiatan.objects.create(nama='pewe')
        kegiatan=Kegiatan.objects.get(nama='pewe')
        self.assertEqual(str(kegiatan),'pewe')
    def test_template_cek(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,'jadwal.html')
