from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def jadwal(request):
    if(request.method == "POST"):
        if 'nama_kegiatan' in request.POST:
            VarA = forms.kegiatan(request.POST)
            if(VarA.is_valid()):
                VarB = models.Kegiatan()
                VarB.nama = VarA.cleaned_data['nama_kegiatan']
                VarB.save()
        elif 'nama_peserta' in request.POST and 'id_kegiatan' in request.POST:
            VarC = forms.peserta(request.POST)
            if(VarC.is_valid()):
                models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                VarB = models.peserta()
                VarB.nama = VarC.cleaned_data['nama_peserta']
                VarB.kegiatan=models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                VarB.save()
    list_kegiatan = models.Kegiatan.objects.all()
    data_kegiatan_lengkap = []
    for kegiatan in list_kegiatan:
        list_peserta = models.peserta.objects.filter(kegiatan = kegiatan)
        data_peserta =[]
        for peserta in list_peserta:
            data_peserta.append(peserta)
        data_kegiatan_lengkap.append((kegiatan,data_peserta))
    return render(request, 'jadwal.html', {'formkegiatan':forms.kegiatan, 'formpeserta':forms.peserta, 'data_kegiatan':data_kegiatan_lengkap})

